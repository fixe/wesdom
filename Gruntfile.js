module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'public_html/public/assets/css/styles.css': 'public_html/public/assets/sass/styles.scss'
                }
            }
        },
        watch: {
            public_html: {
                files: ['public_html/public/assets/**/*.scss'],
                tasks: ['sass:dist'],
                options: {
                    nospawn: true,
                    livereload: true,
                    interrupt: true
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('public_html', ['watch:public_html']);
};